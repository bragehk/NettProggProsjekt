
CREATE DATABASE IF NOT EXISTS HandelUtenHemninger;
USE HandelUtenHemninger;
create table categories
(
    id   bigint       not null
        primary key,
    name varchar(255) not null,
    constraint UK_t8o6pivur7nn124jehx7cygw5
        unique (name)
);

create table categories_seq
(
    next_val bigint null
);

create table listings_seq
(
    next_val bigint null
);

create table users
(
    id           bigint auto_increment
        primary key,
    address      varchar(255) not null,
    email        varchar(255) not null,
    first_name   varchar(255) not null,
    last_name    varchar(255) not null,
    password     varchar(255) not null,
    phone_number bigint       not null,
    role         varchar(255) null,
    constraint UK_6dotkott2kjsp8vw4d0m25fb7
        unique (email)
);

create table conversations
(
    id        bigint auto_increment
        primary key,
    buyer_id  bigint null,
    seller_id bigint null,
    constraint FK11fycx5lvegp8mwplgsl8401h
        foreign key (buyer_id) references users (id),
    constraint FKgcmb2bq98wunnce1y9pyj9sp6
        foreign key (seller_id) references users (id)
);

create table listings
(
    id                          bigint           not null
        primary key,
    address                     varchar(255)     null,
    brief_description           text             not null,
    date_created                datetime(6)      not null,
    description                 text             not null,
    is_current_user_owner       bit              null,
    is_favorite_to_current_user bit              null,
    is_sold                     bit              not null,
    latitude                    double           not null,
    longitude                   double           not null,
    number_of_pictures          int              not null,
    price                       double           not null,
    category_id                 bigint default 1 not null,
    owner_id                    bigint           not null,
    constraint FKawdvt0xd3lqlqyku8qw6m5kou
        foreign key (owner_id) references users (id),
    constraint FKbyfn7t3i7g0f11pcy5veh088p
        foreign key (category_id) references categories (id)
);

create table messages
(
    id              bigint auto_increment
        primary key,
    content         varchar(255) null,
    timestamp       datetime(6)  null,
    conversation_id bigint       null,
    sender_id       bigint       null,
    constraint FK4ui4nnwntodh6wjvck53dbk9m
        foreign key (sender_id) references users (id),
    constraint FKt492th6wsovh1nush5yl5jj8e
        foreign key (conversation_id) references conversations (id)
);

create table user_favourites
(
    user_id    bigint not null,
    listing_id bigint not null,
    constraint FK6thpr9060tvkdritwwjsgx1bl
        foreign key (listing_id) references listings (id),
    constraint FKsotykjapinh9k0qddenalw94k
        foreign key (user_id) references users (id)
);



INSERT INTO HandelUtenHemninger.users (id, address, email, first_name, last_name, password, phone_number, role) VALUES (51, 'SIT Gløs', 'admin@gmail.com', 'Leif', 'admin', '$2a$10$GvN.0AEq0zqCYU9Ik6K.Uuavvil3tKMnnghHMuoBEh6zjGfXemuJS', 1881, 'ADMIN');
INSERT INTO HandelUtenHemninger.users (id, address, email, first_name, last_name, password, phone_number, role) VALUES (52, 'SIT Gløs', 'thor.heyerdahl@gmail.com', 'Thor', 'Heyerdahl', '$2a$10$e4OqQ9bZDkh.qSc33PJIx.ad/NCyaVKtTN9KjEjRjgo449aKpmvFa', 1881, 'ADMIN');
INSERT INTO HandelUtenHemninger.users (id, address, email, first_name, last_name, password, phone_number, role) VALUES (53, 'Dragvoll', 'danger@gmail.com', 'Mister', 'Illegalman', '$2a$10$J4p4BYlHdL0e9qm3S9U/eOkDtaYIYdKA6hx/s0nFFDtAkogZQ47Rq', 1881, 'USER');
INSERT INTO HandelUtenHemninger.users (id, address, email, first_name, last_name, password, phone_number, role) VALUES (54, 'Østlandet tror jeg', 'javadoc@gmail.com', 'Doktor', 'Proktor', '$2a$10$nBBVZTzj6jDinE3PFT6CUucchPvaZN4VjrlAcWYfcT1w2T17BrdfS', 1881, 'USER');
INSERT INTO HandelUtenHemninger.users (id, address, email, first_name, last_name, password, phone_number, role) VALUES (55, 'Dummytown, USA', 'freemoney@msn.com', 'Mr. Beast', 'Rich', '$2a$10$YC8fUGxVBu/KYSfTRG.atebgqKIwac6NwFvP7RkdRtPozCK3Ti3s2', 1337, 'USER');
INSERT INTO HandelUtenHemninger.users (id, address, email, first_name, last_name, password, phone_number, role) VALUES (56, 'Dummytown, USA', 'jeff.bezoz@msn.com', 'Alexa', 'Amazon', '$2a$10$HrZmeRjEaqHebb7SAO9lqeZ.K7pZj.KZZhIyUEm6OqqC.84hEwYnW', 1234567, 'USER');


INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (4, 'books');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (3, 'clothing');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (1, 'electronics');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (2, 'furniture');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (9, 'illegal');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (8, 'misc');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (6, 'sports');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (7, 'tools');
INSERT INTO HandelUtenHemninger.categories (id, name) VALUES (5, 'toys');

INSERT INTO HandelUtenHemninger.categories_seq (next_val) VALUES (501);

INSERT INTO HandelUtenHemninger.listings_seq (next_val) VALUES (601);

INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (505, 'Dybdahls veg 69', 'AK-47', '2023-03-27 21:18:54.025427', 'Hello. I am selling this fine AK-47. Contact me for more info...', false, false, false, 37.7749123, -122.4194123, 4, 5021, 9, 53);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (506, 'Karl Johans gate 420', 'Beautiful chair', '2023-03-27 21:26:17.109504', 'I don''t want this chair anymore. Buy it fast please.', false, false, false, 11.1113, -1337, 1, 125, 2, 55);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (507, 'In the NTNU Catacombs', 'Chess cheating device', '2023-03-27 21:34:03.759109', 'Are you bad at chess? Do you want to take your chess game to the next level? Then look no further! This vibrating shoe gives you the moves, so you don''t have to think. You do have to learn morse code, or another system to be able to cheat, but that is easier than getting good at chess. Good luck and have fun!', false, false, false, 12.34, 4.2, 2, 4799.99, 9, 53);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (508, 'In the NTNU Catacombs', 'Cocaine', '2023-03-27 21:35:47.140283', 'I have a bag of cocaine. Contact for more info.', false, false, false, 12.34, 4.2, 3, 2500.99, 9, 53);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (509, 'In the NTNU Catacombs', 'Credit card', '2023-03-27 21:36:34.069618', 'I found this credit card the other day. You want it?', false, false, false, 12.34, 4.2, 1, 800, 9, 53);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (510, 'In the NTNU Catacombs', 'NTNU Student Card', '2023-03-27 21:37:34.656174', 'I found this student card the other day. You want it? A normal student card costs 150 kr, so this is a really great deal!', false, false, false, 12.34, 4.2, 2, 100, 9, 53);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (511, 'ballefjatten 63', 'Tygggeleke for hund', '2023-03-27 21:39:01.823720', 'Jeg må desvere bli kvitt min avdøde hunds tygge leke ettersom den forsatt vekker store føleser. Denne tyggeleken er godt brukt av min avdøde hund Laika. Hun var en helt fantastisk flott hund med flotte tenner og en realistisk hale. Hvil i fred min elskerde Laika RIP.', false, false, false, 14.531, 56.321, 2, 149.9, 5, 55);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (512, 'North Korea', 'Nuclear Warhead', '2023-03-27 21:39:35.186215', 'For the low low price of only 100 million kr, you can get yourself your very own nuclear warhead! The local authorities might be hassle, but you''ll figure it out.', false, false, false, 999.999, 12345.234, 3, 100000000, 9, 53);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (513, 'North Korea', 'Docker Whale', '2023-03-27 21:43:14.007513', 'I found the docker whale! Alive!!', false, false, false, 9.999, -12345.234, 2, 1337420, 8, 53);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (514, 'North Korea', 'Sweat pants', '2023-03-27 21:45:52.831406', 'Now with more sweat!', false, false, false, 9.999, -12345.234, 2, 600, 3, 56);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (515, 'Øvre Singsakers vei 45', 'Tennis racket or something', '2023-03-27 21:47:04.794164', 'You hit the ball with this.', false, false, false, 92.999, -145.234, 2, 144, 6, 56);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (516, 'Øvre Singsakers vei 45', 'Ny PlayStation!', '2023-03-27 21:48:26.095307', 'Helt ny og uåpnet PlayStation selges.', false, false, false, 92.999, -145.234, 2, 144, 1, 56);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (517, 'Hakkebakkeskogen 43', '?? KUPP! SJELDENT TILBUD! SELGER TUSENLAPP FOR BARE 500 KR! ??', '2023-03-27 21:49:12.204994', 'Er du lei av å ha for mye penger? Vil du doble verdien av pengene dine? Da har du kommet til rett sted! Jeg tilbyr en autentisk, helt ekte tusenlapp for kun 500 kr! Ja, du leste riktig: Halv pris!

? Hvorfor selger jeg en tusenlapp til så lav pris? ?
Vel, det er fordi jeg er en ekstremt generøs person, og jeg vil dele gleden av å ha tusenlapper med andre! Dessuten trenger jeg mer plass i lommeboka mi.

? Hva kan du gjøre med en tusenlapp? ?
Mulighetene er uendelige! Du kan bruke den til å betale for varer og tjenester, veksle den inn for mindre sedler, eller bare vise den frem som et symbol på rikdom og suksess.

? Rask levering! ?
Send meg en melding, og jeg vil sende deg tusenlappen umiddelbart etter at jeg har mottatt betalingen på 500 kr. Du vil motta din splitter nye tusenlapp rett i postkassen din!

⏳ Begrenset tilbud! ⏳
Dette tilbudet vil ikke vare evig! Skynd deg å sikre deg en tusenlapp for bare 500 kr før det er for sent!', false, false, false, 32.123, 34.533, 2, 500, 8, 55);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (518, 'Øvre Singsakers vei 45', 'Cool toy car', '2023-03-27 21:49:13.249044', 'This car is super cool. Lambo go vroom broom vroom!', false, false, false, 92.999, -145.234, 5, 144, 5, 56);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (519, 'Øvre Singsakers vei 45', 'Diary of a wimpy kid', '2023-03-27 21:49:53.900079', 'This guy is not sigma.', false, false, false, 92.999, -145.234, 1, 144, 4, 56);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (521, 'silikon dalen', '?? "Vue 3 for Dummies" - Selges av en som virkelig MISLIKER Vue ??', '2023-03-27 22:05:43.308958', 'Er du en av dem som fremdeles ikke har innsett at Vue er det verste som kunne skje med front-end utvikling? Vel, da er du ikke alene! Som en person som ærlig talt misliker Vue sterkt, tilbyr jeg nå denne boken, "Vue 3 for Dummies", til en "heldig" kjøper!

? Hvorfor selger jeg denne boken? ?
Jeg har fått nok av Vue og alt det står for. Men som en samvittighetsfull borger, føler jeg at det er min plikt å sørge for at andre mennesker i det minste får en sjanse til å lære om denne "vidunderlige" teknologien - selv om jeg personlig mener at det finnes bedre alternativer der ute.

? Hvorfor bør du kjøpe denne boken? ?
Hvis du insisterer på å lære Vue, vil denne boken gi deg en grundig innføring i det "beste" av Vue 3 (ja, ironisk nok). Kanskje du til og med klarer å overbevise meg om at Vue faktisk er verdt noe (men ikke hold pusten).

? Pris: 200 kr (eller bytt den med en bok om React, Angular eller Svelte) ?
Til tross for min åpenbare uvilje mot Vue, vil jeg være så sjenerøs å tilby denne boken til en rimelig pris. Eller hvis du har en bok om et annet rammeverk som ikke suger like mye som Vue, kan vi bytte.', false, false, false, 48.412, 31.34, 2, 200, 4, 55);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (522, 'Hackerspace NTNU', '?? FURRY-FRI SONE! ?? EKSKLUSIV LEGO ATOMBOMBE - NÅ TIL SALGS! ?', '2023-03-27 22:16:02.076746', 'Er du lei av den stadig økende populariteten til furries, og leter etter en "løsning" som kan sette en stopper for denne absurde trenden? Se ikke lenger! Jeg tilbyr nå en 100% fungerende og høyst "seriøs" LEGO atombombe, spesielt designet for å utrydde alt som har med furries å gjøre.

? LEGO ATOMBOMBENS EGENSKAPER ?
Denne eksplosive LEGO-kreasjonen inneholder alt du trenger for å sette en umiddelbar stopper for furry-kulturen! Den er bygget med de fineste plastklossene, og inneholder en "høyst ekte" kjernereaktor. Bare følg den medfølgende bruksanvisningen og se hvordan du eliminerer furry-problemet i et enkelt trinn!

? HVORDAN BRUKE LEGO ATOMBOMBEN ?
Plasser LEGO atombomben i det furry-infiserte området, og se hvordan den "fungerende" atombomben forvandler dette området til et rent og furry-fritt sted. Husk, dette er et "seriøst" produkt og skal tas på høyeste alvor!

? KUN FOR ANSVARSFULLE KJØPERE! ?
Siden dette er en "ekte" og "farlig" LEGO atombombe, forventer vi at bare de mest ansvarsfulle kjøperne vil vise interesse. Vi stoler på at du vil bruke dette produktet med forsiktighet og respekt for andres sikkerhet og velvære.', false, false, false, 34.534, 33.21, 1, 69420, 9, 55);
INSERT INTO HandelUtenHemninger.listings (id, address, brief_description, date_created, description, is_current_user_owner, is_favorite_to_current_user, is_sold, latitude, longitude, number_of_pictures, price, category_id, owner_id) VALUES (523, 'gutterommet', '?? Behold, den ultimate løsningen på dine romantiske problemer: "Flirting for Dummies"! ??', '2023-03-27 22:25:19.786727', 'Er du lei av å leve i en verden der du ikke får den oppmerksomheten du fortjener fra det motsatte kjønn? Har du prøvd alle de "sikre" sjekketriksene, men likevel ikke sett noen resultater? Vel, frykt ikke lenger! Som en selverklært incel, har jeg funnet den perfekte guiden for deg: "Flirting for Dummies"! Og nå kan DU bli eieren av denne boken!

? Hvorfor selger jeg denne boken? ?
Etter å ha studert boken nøye, har jeg innsett at den ikke passer for meg. Jeg foretrekker å leve i min egen verden der jeg kan klandre alle andre for mine romantiske problemer. Men kanskje DU kan lære noe fra denne boken og endelig finne lykken du så desperat søker!

? Hva inneholder "Flirting for Dummies"? ?
Denne boken inneholder alle de hemmelige teknikkene for å mestre kunsten å flørte. Du vil lære hvordan du skal snakke, hva du skal si, og hvordan du skal oppføre deg for å vinne hjertet til din utkårede. Men vær advart: Dette er seriøse metoder, og du bør ikke bruke dem med mindre du er forberedt på å bli en fullverdig "Chad"!

? Pris og betaling ?
Jeg tilbyr denne fantastiske boken for en uslåelig pris på 150 kr. Send meg en melding, og jeg vil sørge for at boken er på vei til deg så snart betalingen er mottatt.', false, false, false, 23.24, 23.55, 1, 150, 4, 55);

import { ref, readonly } from 'vue';

const serverIP = ref('http://localhost:8080');



export function getIp() {
  return {
    serverIP: readonly(serverIP),
  };
}

